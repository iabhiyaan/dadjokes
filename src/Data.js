import React, { Component } from "react";
class Data extends Component {
	state = {};
	render() {
		return (
			<div>
				<ul className="list-group my-4">
					<li className="list-group-item">{this.props.set} </li>
					<li className="list-group-item">{this.props.selectItem} </li>
					<li className="list-group-item">{this.props.all.question} </li>
					{this.props.all.options.map((op, i) => <li className="list-group-item" key={i}> {op} </li>)}
				</ul>
				{/* <ul className="list-group">
                    {
                        Object.keys(this.props.all).map(iKey => [...Array(this.props.all[iKey])].map((_ ,i) => (
                            <li key={i} className="list-group-item"> {this.props.all[iKey]} </li>
                        )) )
                    }
                </ul> */}
			</div>
		);
	}
}

export default Data;
