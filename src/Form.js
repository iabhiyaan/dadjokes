import React, { Component } from "react";
class Form extends Component {
	state = {
		question: "",
		answer1: "",
		answer2: "",
		answer3: "",
		answer4: "",
		image3: "",
		image2: "",
		image1: ""
	};
	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};
	handleSubmit = e => {
		e.preventDefault();
		const submitForm = { ...this.state};
		// console.log(submitForm);
		this.props.submitForm(submitForm);
		this.setState({
			question: "",
			answer1: "",
			answer2: "",
			answer3: "",
			answer4: "",
			image3: "",
			image2: "",
			image1: ""
		});
	};

	render() {
		return (
			<form className="form-row" onSubmit={this.handleSubmit}>
				<div className="form-group col-lg-6">
					<label htmlFor="set"> Set </label>
					<input
						type="text"
						name="set_name"
						value={this.props.set_name}
						onChange={this.props.handleChildChange}
						className="form-control"
						id="set"
					/>
				</div>
				<div className="form-group col-lg-6">
					<label htmlFor="level">Select Level</label>
					<select
						className="form-control"
						onChange={this.props.handleChildChange}
						name="selectItem"
						id="level"
					>
						<option value="">--Select--</option>
						{this.props.levels.map((level, i) => (
							<option key={i} value={level}>
								{level}
							</option>
						))}
					</select>
				</div>
				<div className="col-12">
					<div className="card">
						<div className="card-body row">
							<div className="form-group col-12">
								<label htmlFor="question">Enter question</label>
								<div className="row m-0 p-0">
									<div className="col-8 m-0 p-0">
										<input
											type="text"
											value={this.state.question}
											onChange={this.handleChange}
											className="form-control"
											name="question"
											id="question"
										/>
									</div>
									<div className="col-1 m-0 p-0 align-self-center text-center">OR</div>
									<div className="col-3 m-0 p-0">
										<input
											type="file"
											onChange={this.handleChange}
											value={this.state.image1}
											className="form-control"
											name="image1"
											id="image1"
										/>
									</div>
								</div>
							</div>
							<div className="form-group col-lg-3">
								<label htmlFor="answer1">Answer 1</label>
								<input
									type="text"
									value={this.state.answer1}
									onChange={this.handleChange}
									className="form-control mb-2"
									name="answer1"
									id="answer1"
								/>
								<input
									type="file"
									onChange={this.handleChange}
									value={this.state.image1}
									className="form-control"
									name="image1"
									id="image1"
								/>
							</div>
							<div className="form-group col-lg-3">
								<label htmlFor="answer2">Answer 2</label>
								<input
									type="text"
									value={this.state.answer2}
									onChange={this.handleChange}
									className="form-control mb-2"
									name="answer2"
									id="answer2"
								/>
								<input
									type="file"
									onChange={this.handleChange}
									value={this.state.image2}
									className="form-control"
									name="image2"
									id="image2"
								/>
							</div>
							<div className="form-group col-lg-3">
								<label htmlFor="answer3">Answer 3</label>
								<input
									type="text"
									value={this.state.answer3}
									onChange={this.handleChange}
									className="form-control mb-2"
									name="answer3"
									id="answer3"
								/>
								<input
									type="file"
									onChange={this.handleChange}
									value={this.state.image3}
									className="form-control"
									name="image3"
									id="image3"
								/>
							</div>
							<div className="form-group col-lg-3">
								<label htmlFor="answer4">Correct Answer</label>
								<input
									type="text"
									value={this.state.answer4}
									onChange={this.handleChange}
									className="form-control mb-2"
									name="answer4"
									id="answer4"
								/>
								<input
									type="file"
									onChange={this.handleChange}
									value={this.state.image3}
									className="form-control"
									name="image3"
									id="image3"
								/>
							</div>
						</div>
					</div>
				</div>
				<button type="submit" className="btn btn-primary my-3" onClick={this.clearForm}>
					Add New Question
				</button>
			</form>
		);
	}
}

export default Form;
