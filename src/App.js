import React, { Component } from "react";
import "./App.css";
import axios from "axios";
// import Form from "./Form";
import DataList from "./DataList";
class App extends Component {
	componentDidMount() {
		axios
			.get("https://quiz-ec893.firebaseio.com/questions.json")
			.then(res => console.log(res.data.LoeulTVseQ3daqobGiS))
			.catch(err => console.log(err));
	}

	handleSubmit = e => {};
	render() {
		return (
			<div className="App mt-5">
				<div className="container">
					{/*
					<Form /> */}
					<DataList />
					<div className="FloatingButton">
						<i className="fas fa-plus" />
					</div>
				</div>
			</div>
		);
	}
}

export default App;
