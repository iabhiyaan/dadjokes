import React, { Component } from "react";
import Form from "./Form";
import Data from "./Data";
import axios from "axios";

class DataList extends Component {
	state = {
		questions: [],
		set_name: "",
		selectItem: "",
		level: ["Beginner", "Intermediate", "Advance"]
	};

	submitForm = item => {
		// const allStates = { ...this.state, item };
		const data = {
			question: item.question,
			answers: [item.answer4],
			options: [item.answer1, item.answer2, item.answer3, item.answer4],
			id: item.id
		};
		// console.log(data);
		this.setState({
			questions: [...this.state.questions, data]
		});
	};
	postHandle = () => {
		const postData = {
			questions: this.state.questions,
			set_name: this.state.set_name,
			level: this.state.selectItem
		};
		axios
			.post("https://quiz-ec893.firebaseio.com/questions.json", postData)
			.then(res => {
				console.log(res.data);
				this.setState({ selectItem: "", set_name: "" });
			})
			.catch(err => console.log(err));
	};
	handleChildChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		});
	};
	render() {
		let data = this.state.questions.map((list, i) => (
			<Data key={i} all={list} set={this.state.set_name} selectItem={this.state.selectItem} />
		));
		return (
			<div>
				<Form
					submitForm={this.submitForm}
					levels={this.state.level}
					set_name={this.state.set_name}
					handleChildChange={this.handleChildChange}
					selectItem={this.state.selectItem}
				/>
				{data}
				<button className="btn btn-success" onClick={this.postHandle}>
					Add to database
				</button>
			</div>
		);
	}
}

export default DataList;
