import React, { Component } from "react";
import uuid from "uuid/v4";
import axios from "axios";
import "./JokeList.css";
import Joke from "./Joke";
class JokeList extends Component {
	static defaultProps = {
		numToGet: 10
	};
	state = {
		jokes: JSON.parse(window.localStorage.getItem("jokes" || "[]")),
		loading: false
	};
	constructor(props) {
		super(props);
		this.seenJokes = new Set(this.state.jokes.map(joke => joke.text));
		console.log(this.seenJokes);
	}
	componentDidMount() {
		if (this.state.jokes.length === 0) {
			this.getJokes();
		}
	}
	handleGetJoke = () => {
		this.setState({ loading: true }, this.getJokes);
	};
	async getJokes() {
		try {
			let jokes = [];
			while (jokes.length < this.props.numToGet) {
				let res = await axios.get("https://icanhazdadjoke.com/", { headers: { Accept: "application/json" } });
				let newJokes = res.data.joke;
				if (!this.seenJokes.has(newJokes)) {
					jokes.push({ text: newJokes, votes: 0, id: uuid() });
				} else {
					console.log("Duplicate Found");
					console.log(newJokes);
				}
			}
			this.setState(
				cState => ({
					jokes: [...cState.jokes, ...jokes],
					loading: false
				}),
				() => window.localStorage.setItem("jokes", JSON.stringify(this.state.jokes))
			);
		} catch (e) {
			alert(e);
			this.setState({
				loading: false
			});
		}
	}
	handleVote(id, delta) {
		this.setState(
			currState => ({
				jokes: currState.jokes.map(joke => (joke.id === id ? { ...joke, votes: joke.votes + delta } : joke))
			}),
			() => window.localStorage.setItem("jokes", JSON.stringify(this.state.jokes))
		);
	}
	render() {
		let jokes = this.state.jokes.sort((a, b) => b.votes - a.votes);
		if (this.state.loading) {
			return (
				<div className="JokeList-spinner">
					<i className="far fa-spin fa-8x fa-laugh" />
					<h1 className="Joke-title">Loading...</h1>
				</div>
			);
		}
		return (
			<div className="JokeList">
				<div className="JokeList-sidebar">
					<h1 className="JokeList-title">Dad Jokes</h1>
					<img src="https://assets.dryicons.com/uploads/icon/svg/8927/0eb14c71-38f2-433a-bfc8-23d9c99b3647.svg" />
					<button className="JokeList-getmore" onClick={this.handleGetJoke}>
						New Jokes
					</button>
				</div>
				<div className="JokeList-jokes">
					{jokes.map(joke => (
						<Joke
							upVotes={() => this.handleVote(joke.id, 1)}
							downVotes={() => this.handleVote(joke.id, -1)}
							key={joke.id}
							votes={joke.votes}
							text={joke.text}
						/>
					))}
				</div>
			</div>
		);
	}
}

export default JokeList;
